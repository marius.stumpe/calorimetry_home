"""
Praktikum Digitalisierung

Kalorimetrie - Küchentischversuch

Marius Stumpe
"""

from functions import m_json
from functions import m_pck



""" heat capacity """

path_heat_capacity = "datasheets/setup_heat_capacity.json"
metadata_heat_capacity = m_json.get_metadata_from_setup(path_heat_capacity)

folder_path = "datasheets"
m_json.add_temperature_sensor_serials(folder_path, metadata_heat_capacity)

data_heat_capacity = m_pck.get_meas_data_calorimetry(metadata_heat_capacity)

data_folder_heat_capacity = "data/heat_capacity"
m_pck.logging_calorimetry(data_heat_capacity, metadata_heat_capacity, data_folder_heat_capacity, folder_path)

m_json.archiv_json(folder_path, path_heat_capacity, data_folder_heat_capacity)


""" newton """

path_newton = "datasheets/setup_newton.json"
metadata_newton = m_json.get_metadata_from_setup(path_newton)

folder_path = "datasheets"
m_json.add_temperature_sensor_serials(folder_path, metadata_newton)

data_newton = m_pck.get_meas_data_calorimetry(metadata_newton)

data_folder_newton = "data/newton"
m_pck.logging_calorimetry(data_newton, metadata_newton, data_folder_newton, folder_path)

m_json.archiv_json(folder_path, path_newton, data_folder_newton)